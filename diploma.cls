\ProvidesClass{diploma} 
\LoadClass[a4paper,8pt,twoside]{book} % ładujemy klasę bazową, do opcji

\usepackage[polish]{babel} %włączenie paczki językowej babel
\usepackage[utf8]{inputenc} %ustawienie kodowania na utf8
\usepackage[T1]{fontenc}  %obsługa polskich znaków
\usepackage{graphicx} %włączenie biblioteki do wczytywania obiektów graficznych
\usepackage{hyperref} %włączenie biblioteki umożliwiającej działanie na hyperlinkach
\usepackage{color} %włączenie biblioteki do kolorowania tekstu
\usepackage{graphics} %włączenie biblioteki przetwarzającej obrazy
\usepackage{listings} %włączenie pakietu do umieszczania kodu źródłowego
\usepackage[usenames,dvipsnames]{xcolor}

%Metadane dokumentu
\author{Daniel Drożdż}
\title{Praca dyplomowa}

%ustawienia akapitu
\setlength{\parindent}{25pt} %lewe wcięcie akapitu

%zmiany definicji wyrażeń
\def\contentsname{Spis treści:} %nazwanie sekcji spisu treści

%ustawienia pliku pdf
\hypersetup{pdfauthor={Daniel Drożdż},pdftitle={Diploma} }

%definicje nazw kolorów
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

%ustawienia pakietu listings (wyświetlanie kodu źródłowego)
\lstset{ %
  language=C++,                   % the language of the code
  basicstyle=\tiny,       		  % the size of the fonts that are used for the code
  numbers=left,                   % where to put the line-numbers
  numberstyle=\tiny\color{gray},  % the style that is used for the line-numbers
  stepnumber=1,                   % the step between two line-numbers. If it's 1, each line 
                                  % will be numbered
  numbersep=5pt,                  % how far the line-numbers are from the code
  backgroundcolor=\color{white},  % choose the background color. You must add \usepackage{color}
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  frame=single,                   % adds a frame around the code
  rulecolor=\color{black},        % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  tabsize=1,                      % sets default tabsize to 2 spaces
  captionpos=b,                   % sets the caption-position to bottom
  breaklines=true,                % sets automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  title=\lstname,                 % show the filename of files included with \lstinputlisting;
                                  % also try caption instead of title
  keywordstyle=\color{blue},      % keyword style
  commentstyle=\color{dkgreen},   % comment style
  stringstyle=\color{mauve},      % string literal style
  escapeinside={\%*}{*)},         % if you want to add LaTeX within your code
  morekeywords={*,...}            % if you want to add more keywords to the set
}

\endinput % dla porządku
