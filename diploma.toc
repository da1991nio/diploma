\select@language {polish}
\contentsline {chapter}{\numberline {1}Wprowadzenie}{8}{chapter.1}
\contentsline {section}{\numberline {1.1}Cel pracy}{8}{section.1.1}
\contentsline {section}{\numberline {1.2}Zakres pracy}{8}{section.1.2}
\contentsline {section}{\numberline {1.3}Analiza istniej\IeC {\k a}cych rozwi\IeC {\k a}za\IeC {\'n}}{9}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Program Hide Wizard}{9}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Program BlindBossKey}{10}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Driver to Hide Processes and Files - Hide Driver}{10}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}Podsumowanie analizy}{12}{subsection.1.3.4}
\contentsline {chapter}{\numberline {2}Podstawy teoretyczne}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Architektura Windows NT}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}Ukrywanie proces\IeC {\'o}w}{16}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Punkty zaczepienia}{17}{subsection.2.2.1}
\contentsline {subsubsection}{Punkty zaczepienia w j\IeC {\k a}drze systemu}{17}{section*.6}
\contentsline {subsubsection}{Punkty zaczepienia w tablicy deskryptor\IeC {\'o}w us\IeC {\l }ug systemowych}{18}{section*.7}
\contentsline {subsubsection}{Modyfikacja zabezpiecze\IeC {\'n} SSDT z wykorzystaniem Memory Descriptor List.}{19}{section*.8}
\contentsline {subsubsection}{Ukrywanie proces\IeC {\'o}w z wykorzystaniem SSDT}{21}{section*.9}
\contentsline {subsection}{\numberline {2.2.2}Bezpo\IeC {\'s}rednie manipulacje na obiektach j\IeC {\k a}dra (DKOM)}{23}{subsection.2.2.2}
\contentsline {subsubsection}{Okre\IeC {\'s}lenie wersji systemu operacyjnego}{25}{section*.10}
\contentsline {subsubsection}{Ukrywanie proces\IeC {\'o}w z wykorzystaniem DKOM}{27}{section*.11}
\contentsline {section}{\numberline {2.3}Ukrywanie plik\IeC {\'o}w wykonywalnych.}{31}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Punkty zaczepienia}{32}{subsection.2.3.1}
\contentsline {subsubsection}{Modyfikacja wynik\IeC {\'o}w zwracanych przez pierwotne funkcje.}{36}{section*.12}
\contentsline {subsubsection}{Ukrycie wybranego pliku}{39}{section*.13}
\contentsline {section}{\numberline {2.4}Podsumowanie rozdzia\IeC {\l }u}{43}{section.2.4}
\contentsline {chapter}{\numberline {3}Wykorzystane narz\IeC {\k e}dzia}{44}{chapter.3}
\contentsline {section}{\numberline {3.1}J\IeC {\k e}zyki programowania - C,C++}{44}{section.3.1}
\contentsline {section}{\numberline {3.2}Microsoft Visual Studio 2008}{44}{section.3.2}
\contentsline {section}{\numberline {3.3}Windows Driver Developer Kit 2003}{45}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}WinDBG}{45}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Biblioteka Boost}{46}{section.3.4}
\contentsline {section}{\numberline {3.5}Sparx Enterprise Architect}{47}{section.3.5}
\contentsline {section}{\numberline {3.6}System kontroli wersji Git}{47}{section.3.6}
\contentsline {section}{\numberline {3.7}Podsumowanie rozdzia\IeC {\l }u}{49}{section.3.7}
\contentsline {chapter}{\numberline {4}Za\IeC {\l }o\IeC {\.z}enia projektowe, wymagania oraz projekt aplikacji}{50}{chapter.4}
\contentsline {section}{\numberline {4.1}Za\IeC {\l }o\IeC {\.z}enia projektowe i warunki wst\IeC {\k e}pne}{50}{section.4.1}
\contentsline {section}{\numberline {4.2}Wymagania dotycz\IeC {\k a}ce aplikacji}{52}{section.4.2}
\contentsline {section}{\numberline {4.3}Opis wymaga\IeC {\'n}}{53}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Wymaganie niefunkcjonalne}{54}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Wymagania funkcjonalne}{54}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Projekt aplikacji}{57}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Model przypadk\IeC {\'o}w u\IeC {\.z}ycia}{58}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Model statyczny}{59}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Model dynamiczny}{65}{subsection.4.4.3}
\contentsline {section}{\numberline {4.5}Podsumowanie rozdzia\IeC {\l }u}{67}{section.4.5}
\contentsline {chapter}{\numberline {5}Implementacja i testowanie aplikacji}{68}{chapter.5}
\contentsline {section}{\numberline {5.1}Krytyczne miejsca w kodzie}{68}{section.5.1}
\contentsline {section}{\numberline {5.2}Trudno\IeC {\'s}ci implementacje}{70}{section.5.2}
\contentsline {section}{\numberline {5.3}Testowanie aplikacji}{71}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Testy funkcjonalne}{71}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Testy niefunkcjonalne}{72}{subsection.5.3.2}
\contentsline {subsubsection}{Testy bezpiecze\IeC {\'n}stwa}{73}{section*.14}
\contentsline {subsubsection}{Testy wydajno\IeC {\'s}ciowe}{78}{section*.15}
\contentsline {chapter}{\numberline {6}Wnioski ko\IeC {\'n}cowe}{82}{chapter.6}
